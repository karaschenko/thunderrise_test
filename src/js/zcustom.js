$(document).ready(function() {
    $('.hamburger-menu').click(function(){
        $(this).toggleClass('open');
        $('.nav').toggleClass('active');

    });
    $('.main-month-list-item').on('click',function() {
        $(this).closest('.col-md-4').siblings().find('.main-month-list-item').removeClass('active');
        $(this).toggleClass('active');
    });
    $('.main-picker__item').on('click',function() {
        $(this).closest('.col-md-1').siblings().find('.main-picker__item').removeClass('active');
        $(this).toggleClass('active');
    });
    $('.main-sort-list__item').on('click',function() {
        $(this).closest('.col-md-2').siblings().find('.main-sort-list__item').removeClass('active');
        $(this).toggleClass('active');
    });
})
